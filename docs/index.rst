Welcome to Tesuto Python's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Installation

   installation/index

.. toctree::
   :maxdepth: 2
   :caption: Modules

   api/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
