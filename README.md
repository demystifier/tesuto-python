# Tesuto Python Library

![coverage](https://gitlab.com/tesuto/public/tesuto-python/badges/master/coverage.svg?job=coverage)

## Documentation

See the [API docs](https://developer.tesuto.com).

## Installation

Unless you want to modify this source code just install the package:

    pip install git+https://gitlab.com/tesuto/public/tesuto-python@master#egg=tesuto_python

Install from source with:

    python setup.py install

### Requirements

* Python 3.4+

## Usage

The library needs to be configured with your account's secret token which is
available in your [Tesuto Cloud Console][api-tokens]. Set `tesuto.api_token` to its
value:

``` python
from tesuto import tesuto

tesuto.config.set('api_token', '...')

# list emulations
emulations = tesuto.apis.Emulation.list()

# filter emulations
emulations = tesuto.apis.Emulation.list(params="filter=(emulation_id eq 1234)")

# retrieve an emulation
em = tesuto.apis.Emulation.retrieve("1234").data
tesuto.console.to_json(em)

# update an emulation
tesuto.apis.Emulation.put(em.id, data={'name': 'myemulation'})

# create an emulation
tesuto.apis.Emulation.post(data={'name': 'myemulation'})
```
### Logging

The library can be configured to emit logging that will give you better insight
into what it's doing. The `info` logging level is usually most appropriate for
production use, but `debug` is also available for more verbosity.

Enable it through Python's logging module:
   ```py
   import logging
   logging.basicConfig()
   logging.getLogger('tesuto').setLevel(logging.DEBUG)
   ```

## Development

Run all tests on all supported Python versions:

    make test

Run all tests for a specific Python version (modify `-e` according to your Python target):

    pipenv run tox -e py3

Run all tests in a single file:

    pipenv run tox -e py3 -- tests/resources/emulations.py

Run a single test suite:

    pipenv run tox -e py3 -- tests/resources/emulations.py::TestEmulation

Run a single test:

    pipenv run tox -e py3 -- tests/resources/emulations.py::TestEmulation::test_list

Run the linter with:

    make lint

[api-tokens]: https://cloud.tesuto.com/security/tokens
