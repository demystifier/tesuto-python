import logging
from tesuto.core import const
from tesuto.apis import Emulation, EmulationTopology, EmulationDevice

logging.getLogger(const.LOGGER).setLevel(logging.WARNING)
logging.getLogger('urllib3').setLevel(logging.WARNING)

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
logging.info("Retrieving Emulations")

for emulation in Emulation.list().data:
    print("Emulation:", emulation.id)
    devices = EmulationDevice.list([emulation.id]).data
    for device in devices:
        print("{:>2s} {}:".format('', device.name))
        print("{:>5s} {}: {}".format('', 'id', device.id))
        print("{:>5s} {}: {}".format('', 'interfaces', device.interfaces))

    topologies = EmulationTopology.list([emulation.id]).data
    if topologies:
        print("\n{:>2s} {}:".format('', 'Topologies'))
        for topo in topologies:
            print('{:>5s} {device} {interface} <=> '
                  '{neighbor} {neighbor_interface}'.format('', **topo))
