from tesuto.apis import Job


class TestJob(object):
    def test_list(self):
        resp = Job.list(missing=True)
        assert resp.status_code == 200
        assert isinstance(resp.data, list)

    def _test_id(self):
        resp = Job.get(12345, missing=True)
        assert resp.status_code == 404

        resp = Job.list(missing=True)
        resp = Job.get(resp.data[0].id, missing=True)
        assert resp.status_code == 200
        assert isinstance(resp.data, Job)
