from tesuto.apis import Organization


class TestOrganization(object):
    def test_list(self):
        resp = Organization.list(missing=True)
        assert resp.status_code == 200
        assert isinstance(resp.data, list)
        assert isinstance(resp.data[0], Organization)
        assert isinstance(resp.data[0].id, int)
