from tesuto.apis import Emulation, EmulationDevice


class TestEmulationDevice(object):
    def test_id(self):
        emulations = Emulation.list(missing=True)
        assert emulations.status_code == 200

        for emulation in emulations.data:
            assert isinstance(emulation.id, int)
            devices = EmulationDevice.list(map_args=[emulation.id])
            assert isinstance(devices.data, list)
            for device in devices.data:
                assert isinstance(device.id, int)

                # Test that we can get the device by id
                d = EmulationDevice.get(device.id, map_args=[emulation.id])
                assert isinstance(d.data.id, int)
                assert d.data.id == device.id
