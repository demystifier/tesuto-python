import pytest

from tesuto.core.configs import config


class TestConfig(object):

    def test_nosection_default(self):
        with pytest.raises(KeyError):
            config.get('novar', section='nosection', default='test') == 'test'

    def test_default(self):
        assert config.get('novar', default='test') == 'test'

    def test_section_with_default(self):
        assert config.get('novar', section='test', default='test') == 'test'

    def test_section(self):
        assert config.get('testvar', section='test') == 'test_var'

    def test_ini(self):
        assert config.get('testvar', section='default') == 'test_default'

    def test_set_get_defaults(self):
        config['new_var'] = 'test_new_var'
        assert config.get('new_var', section='default') == 'test_new_var'
        assert config.get('new_var') == 'test_new_var'

    def test_set_var_new_section(self):
        config.set('test_var', 'test_new_var', section='new_var_section')
        assert config.get('test_var', section='new_var_section') == 'test_new_var'

    def test_length(self):
        assert len(config) > 0

    def test_iterable(self):
        config['test_iter'] = 'yes'
        assert len(list(filter(lambda i: i == 'test_iter', config))) == 1

    def test_custom_ini(self):
        config.load_config('tests/test.ini', namespace='new_namespace')
        assert config.get('testvar', section='default',
                          namespace='new_namespace') == 'test_default'

    def test_dup_namespace(self):
        config.load_config('tests/test.ini', namespace='dup_namespace')
        with pytest.raises(KeyError):
            config.load_config('tests/test.ini', namespace='dup_namespace')

    def test_boolean(self):
        config.load_config('tests/test.ini', namespace='test_namespace')
        assert config.getboolean('debug', namespace='test_namespace') is True
        assert config.getboolean('bool1', namespace='test_namespace') is True
        assert config.getboolean('bool2', namespace='test_namespace') is True
        assert config.getboolean('bool3', namespace='test_namespace') is True

    def test_int(self):
        config['test_int'] = "1"
        assert config.getint('test_int') == 1

    def test_float(self):
        config['test_float'] = "0.01"
        assert config.getfloat('test_float') == 0.01

    def test_no_namespace(self):
        with pytest.raises(KeyError):
            config.get_namespace('not_a_namespace')

    def test_interpol(self):
        assert config.get('test1', section='test-interpol') is not None
        assert config.get('test1', section='test-interpol') != '$HOME'
        assert config.get('test1', section='test-interpol').endswith('/test')
        assert config.get('test2', section='test-interpol').endswith('tester')

    def test_setdefault(self):
        config.setdefault('test_default', 'default')
        assert config.get('test_default') == 'default'
        config.setdefault('test_default', 'default2')
        assert config.get('test_default') == 'default'
        config.set('test_default', 'default2')
        assert config.get('test_default') == 'default2'

    def test_setdefault_ini_set(self):
        # Test that setdefault doesn't overwrite the ini config
        assert config.get('testdefault', section='test') == 'test_default'
        config.setdefault('testdefault', 'overwritten', section='test')
        assert config.get('testdefault', section='test') == 'test_default'
        config.set('testdefault', 'overwritten', section='test')
        assert config.get('testdefault', section='test') == 'overwritten'

    def test_sections(self):
        config.add_section('new_section')
        assert len(list(config.items('new_section'))) == 0
