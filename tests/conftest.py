import sys
from tesuto.resources import request
from tesuto.core.const import API_BASE, API_TOKEN

# Make sure the api is accessible before continuing with tests.
resp = request('version', API_BASE, API_TOKEN)
try:
    resp = request('version', API_BASE, API_TOKEN)
except Exception as e:
    sys.exit("Couldn't reach tesuto api at `{0}`. {1}".format(API_BASE, e))
