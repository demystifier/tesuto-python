import os
import sys
import pytest
import logging
import re

from contextlib import contextmanager
from io import StringIO
from tesuto.core.logger import configure_logging


@contextmanager
def reader(stdout: bool = True, stderr: bool = True):
    # Use stringio to store output
    if stdout:
        out = StringIO()
    if stderr:
        err = StringIO()

    sys_out, sys_err = sys.stdout, sys.stderr

    try:
        # Use StringIO stdout, stderr
        if stdout:
            sys.stdout = out
        if stderr:
            sys.stderr = err
        yield sys.stdout, sys.stderr
    finally:
        # Reset stdout and stderr back to sys
        sys.stdout, sys.stderr = sys_out, sys_err


class TestLogger(object):

    @pytest.fixture(autouse=True)
    def setup_logging(self):
        self.logfile = '/tmp/tesuto.test.log'
        yield
        if os.path.exists(self.logfile):
            os.unlink(self.logfile)

    def test_file_logger(self):
        configure_logging(log_file=self.logfile, log_handler='file')
        logging.error('test-msg')
        did_run = False

        with open(self.logfile) as fh:
            lines = fh.readlines()
            assert len(lines) > 0
            for line in lines:
                m = re.search(r'test-msg', line)
                if m:
                    did_run = True
        assert did_run is True

    def test_debug_file_logger(self):
        configure_logging(log_file=self.logfile, log_handler='file', debug=True)
        logging.debug('test-debug-msg')
        did_run = False

        with open(self.logfile) as fh:
            lines = fh.readlines()
            assert len(lines) > 0
            for line in lines:
                m = re.search(r'test-debug-msg', line)
                if m:
                    did_run = True
        assert did_run is True
