from tesuto.core import errors


class TestConnectionError(object):
    def test_invalid_token(self):
        err = errors.ConnectionError('Test Message', retry=True)
        assert err.retry is True
