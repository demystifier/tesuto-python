import logging

from json.decoder import JSONDecodeError
from tesuto.core import errors
from tesuto.core.console import Console
from tesuto.core.configs import config
from tesuto.resources import Resource

console = Console()


class ResponseObject(object):
    status_code = None
    message = None
    data = {}
    id = None
    type = object

    def __init__(self, _type: object = object):
        self.type = _type

    def request(self, endpoint: str, schema: object, method: str = 'GET',
                mapper: str = None, validate_type: object = None,
                missing: bool = None, params: dict = {}, data: dict = {}):
        self._endpoint = endpoint
        req = Resource()
        resp = req.request(method=method, endpoint=endpoint, params=params, data=data)
        logging.debug("%s:%s <params: %s> <data: %s>", method, endpoint, params, data)

        try:
            js_data = resp.json()
        except JSONDecodeError:
            js_data = {}

        if config.getboolean('DEBUG_JSON'):
            console.display("--FULL DATA RESPONSE--")
            console.to_json(js_data)
            console.display("--END DATA RESPONSE--")

        js_data = self._mapper(js_data, mapper)
        self.check_type(js_data, validate_type)
        self.status_code = resp.status_code

        # Using the child class build a list of `class` objects.
        if self.type == list:
            results = []
            if isinstance(js_data, list):
                for item in js_data:
                    obj = self.populate(schema, item, missing=missing)
                    obj._url = self._endpoint
                    results.append(obj)
            self.data = results
        elif self.type == object:
            self.data = self.populate(schema, js_data, missing=missing)
            self.data._url = self._endpoint
        return self

    def _mapper(self, data, mapper):
        # Exact the data to serialize by on accesssor if any.
        # If the mapper is a list then try all the accessors.
        if isinstance(mapper, list):
            for _mapper in mapper:
                if _mapper in data:
                    return data[_mapper]
        elif mapper and mapper in data:
            return data[mapper]
        return data

    def populate(self, schema: object, data: dict, missing: bool = None):
        # Populate the data structure with the response data.
        mapped = schema(data, strict=False)
        missing = missing or config.getboolean('LOAD_MISSING')

        # Only override mapping behavior to include all fields if missing is True,
        # this may be desired behavior if the model schema is out of date with the API.
        if missing:
            lazy = schema(data, strict=False, lazy=True)
            for k in lazy._data:
                if not hasattr(mapped, k):
                    mapped._data[k] = lazy._data[k]
                    setattr(mapped, k, lazy._data[k])
        return mapped

    def check_type(self, data: dict, mapper: str = None, validate_type: object = None):
        # Helper method to extract the API data structure type.
        if validate_type:
            if not isinstance(data, validate_type):
                raise errors.InvalidType(message="<{} not {}>".format(type(data), validate_type))
