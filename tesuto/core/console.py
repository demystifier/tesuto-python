"""
Console output formatting.
"""
import sys
import json
import csv

from datetime import datetime
from json import JSONEncoder

from schematics.models import ModelMeta


class ResponseEncoder(JSONEncoder):

    def default(self, o):
        if isinstance(o.__class__.__base__, ModelMeta):
            return dict(o._data)
        elif isinstance(o, datetime):
            return str(o)
        else:
            return super().default(o)


class Console(object):

    def __init__(self):
        self.widths = {}

    def json_dump(self, data, sort_keys: bool = True, indent: int = 4):
        sys.stdout.write(json.dumps(data, cls=ResponseEncoder, sort_keys=sort_keys, indent=indent))
        sys.stdout.write('\n')

    def _extractor(self, data):
        if isinstance(data.__class__.__base__, ModelMeta):
            return dict(data._data)
        elif isinstance(data, list):
            # Extract any ModelMeta objects before making a dict
            results = []
            for item in data:
                results.append(self._extractor(item))
            return self.to_dict(results)
        elif isinstance(data, object):
            return self.to_dict(data)
        else:
            return data

    def display(self, msg, try_formatting: bool = False):
        if try_formatting:
            results = self._extractor(msg)
            if isinstance(results, list):
                for r in results:
                    sys.stdout.write('{}\n'.format(r))
            else:
                sys.stdout.write('{}\n'.format(results))
        else:
            sys.stdout.write('{}\n'.format(msg))

    def output(self, data, headers: list = [], include_headers: bool = True,
               fmt: str in ['json', 'csv', 'raw'] = 'json', trim: int = None,
               delimiter: str = '|'):
        if '_all' in headers:
            headers = []

        if fmt == 'csv':
            self.to_csv(
                data, trim=trim, headers=headers,
                write_headers=include_headers, delimiter=delimiter
            )
        elif fmt == 'json':
            self.to_json(data)
        else:
            self.display(data, try_formatting=True)

    def to_dict(self, obj: object):
        if isinstance(obj, str):
            return {'data': obj, 'type': 'string'}
        elif isinstance(obj, list):
            return {
                'type': 'array',
                'data': obj
            }
        elif isinstance(obj, dict):
            # already a dict
            return obj
        else:
            result = {}
            members = [
                a for a in dir(obj) if not callable(getattr(obj, a)) and not a.startswith("__")
            ]
            for key in members:
                val = getattr(obj, key)
                try:
                    json.dumps(val)
                except Exception:
                    val = '<{}>'.format(val)
                result[key] = val
            return result

    def to_json(self, a_dict):
        results = self._extractor(a_dict)
        self.json_dump(results, sort_keys=True, indent=4)

    def _csv_writerow(self, writer, row, headers: list = [], trim: int = None):
        results = self._extractor(row)
        results = self._filter_row(results, headers, trim)
        writer.writerow(results)

    def _filter_row(self, data, headers: list = [], trim: int = None):
        keys = data.copy().keys()
        for k in keys:
            if k not in headers:
                del data[k]
            elif trim:
                data[k] = self._trim_value(data[k], trim)
        return data

    def to_csv(self, a_dict: dict, headers: list = [], write_headers: bool = True,
               exclude: list = [], trim: int = None, delimiter='|'):

        if isinstance(a_dict, dict) or isinstance(a_dict.__class__.__base__, ModelMeta):
            headers = headers or a_dict.keys()
            headers = [key for key in headers if key not in exclude]
            writer = csv.DictWriter(sys.stdout, headers, delimiter=delimiter)
            if write_headers:
                writer.writeheader()
            self._csv_writerow(writer, a_dict, headers, trim)

        elif isinstance(a_dict, list):
            if len(a_dict) == 0:
                return

            if isinstance(a_dict[0].__class__.__base__, ModelMeta):
                headers = headers or a_dict[0]._data.keys()
            else:
                headers = headers or a_dict[0].keys()

            headers = [key for key in headers if key not in exclude]
            writer = csv.DictWriter(sys.stdout, headers, delimiter=delimiter)
            writer.field_size_limit = 1
            if write_headers:
                writer.writeheader()
            for item in a_dict:
                self._csv_writerow(writer, item, headers, trim)

    def _trim_value(self, value: str, trim: int = None):
        # Trim integers and string
        if trim and value:
            # boolean could be int so check for that
            if isinstance(value, (str, int)) and not isinstance(value, bool):
                if len(str(value)) > trim:
                    value = str(value)[0:trim] + '...'
        return value
