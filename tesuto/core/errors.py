"""
Errors
"""
from requests.exceptions import ConnectionError as RequestsConnectionError


class TesutoError(Exception):

    def __init__(self, message=None, http_body=None, status_code=None,
                 json_body=None, headers=None, code=None):
        super(TesutoError, self).__init__(message)

        if http_body and hasattr(http_body, 'decode'):
            try:
                http_body = http_body.decode('utf-8')
            except BaseException:
                http_body = ('<Could not decode body as utf-8. '
                             'Please report to support@tesuto.com>')

        self._message = message
        self.http_body = http_body
        self.status_code = status_code
        self.json_body = json_body
        self.headers = headers or {}
        self.code = code
        self.request_id = self.headers.get('request-id', None)

    def __str__(self):
        msg = self._message or "<empty message>"
        if self.request_id is not None:
            return u"Request {0}: {1}".format(self.request_id, msg)
        else:
            return msg

    @property
    def resp_message(self):
        # Returns Exception message, typically from the Tesuto API.
        return self._message

    def __repr__(self):
        return ('{klass}({message}, {status_code})'.format(
            klass=self.__class__.__name__, message=self._message, status_code=self.status_code
        ))


class APIError(TesutoError):
    pass


class ConnectionError(TesutoError, RequestsConnectionError):
    def __init__(self, message, http_body=None, status_code=None,
                 json_body=None, headers=None, code=None, retry=False):
        super(ConnectionError, self).__init__(message, http_body,
                                              status_code,
                                              json_body, headers, code)
        self.retry = retry


class ErrorWithParams(TesutoError):

    def __repr__(self):
        return ('{klass}({message}, {params}, {status_code})'.format(
            klass=self.__class__.__name__, message=self._message,
            params=self.params, status_code=self.status_code
        ))


class InvalidRequestError(ErrorWithParams):

    def __init__(self, message, param, code=None, http_body=None,
                 status_code=None, json_body=None, headers=None):
        super(InvalidRequestError, self).__init__(
            message, http_body, status_code, json_body,
            headers, code)
        self.param = param


class AuthenticationError(TesutoError):
    pass


class PermissionError(TesutoError):
    pass


class RateLimitError(TesutoError):
    pass


class InvalidType(TesutoError):
    pass
