"""
Helper Utils
"""
import re
import os
import string
import inspect


def sum_format_args(s: str) -> int:
    """Formatter string to sum arguments."""
    return sum(i[1] is not None for i in list(string.Formatter().parse(s)))


def boolean(v: str or int) -> bool or None:
    """String or Integer to convert to boolean."""
    if v in ('True', 'true', 1, '1', True):
        return True
    elif v in ('False', 'false', 0, '0', False):
        return False


def snake_to_camel(v: str) -> str:
    if '_' in v:
        return ''.join(x.capitalize() or '_' for x in v.split('_'))
    else:
        return '{}{}'.format(v[0].upper(), v[1:])


def camel_to_snake(v: str) -> str:
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', v)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def find_classes(module, klass: object = None):
    """Find all classes in a given module, if klass is defined retrict match to that type."""
    classes = []
    for i in dir(module):
        attribute = getattr(module, i)

        if inspect.isclass(attribute):
            if klass and issubclass(attribute, klass):
                classes.append(attribute)
    return classes


def has_pid(pid=0):
    if not pid or pid <= 0:
        return False
    else:
        try:
            os.kill(pid, 0)
        except PermissionError:
            # found a pid with another owner
            return True
        except ProcessLookupError:
            # Couldn't find pid
            return False
        else:
            return True
