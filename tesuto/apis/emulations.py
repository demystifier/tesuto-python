from tesuto.models import BaseModel, types


class EmulationTopology(BaseModel):
    id = types.IntType()
    device = types.StringType()
    device_id = types.IntType()
    interface = types.StringType()
    neighbor = types.StringType()
    neighbor_id = types.IntType()
    neighbor_interface = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'emulations/{0}/topologies'
        __mapper__ = 'data'
        __mapper_post__ = None
        __params__ = {'linked': True}


class EmulationDevice(BaseModel):
    name = types.StringType()
    slug = types.StringType()
    vendor_name = types.StringType()
    vendor_id = types.IntType()
    model_name = types.StringType()
    model_id = types.StringType()
    version_id = types.StringType()
    version_name = types.StringType()
    snapshot_id = types.StringType()
    snapshot_name = types.StringType()
    config_id = types.StringType()
    can_snapshot = types.BooleanType()
    emulation_id = types.IntType()
    interfaces = types.ListType(types.StringType)
    is_enabled = types.BooleanType(default=True)

    class Options(BaseModel.Options):
        __resource__ = 'emulations/{0}/devices'
        __mapper__ = 'data'


class EmulationAccess(BaseModel):
    emulation_id = types.StringType()
    protocol = types.StringType()
    port = types.StringType()
    source_ip = types.StringType()
    action = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'emulations/{0}/access'
        __mapper__ = 'access'


class EmulationValidator(BaseModel):
    script_id = types.IntType()
    script_name = types.StringType()
    script_module = types.StringType()
    framework_id = types.IntType()
    framework_name = types.StringType()
    comment = types.StringType()
    parse_params = types.StringType()
    state_params = types.StringType()
    active = types.BooleanType()

    class Options(BaseModel.Options):
        __resource__ = 'emulations/{0}/validators'
        __mapper__ = 'validators'


class Emulation(BaseModel):
    name = types.StringType()
    slug = types.StringType()
    region = types.StringType()
    region_id = types.IntType()
    is_managed = types.BooleanType(default=True)
    is_persistent = types.BooleanType(default=True)
    is_ztp = types.BooleanType(default=True)
    status = types.StringType()

    @types.serializable
    def devices(self):
        return self._serialize_list(mapper=['devices', 'data'], model=EmulationDevice)

    @types.serializable
    def access(self):
        return self._serialize_list(mapper=['access', 'data'], model=EmulationAccess)

    @types.serializable
    def topologies(self):
        return self._serialize_list(mapper=['topologies', 'data'], model=EmulationTopology)

    @types.serializable
    def validators(self):
        return self._serialize_list(
            mapper=['validators', 'data'], model=EmulationValidator
        )

    class Options(BaseModel.Options):
        __resource__ = 'emulations'
        __mapper__ = 'data'
