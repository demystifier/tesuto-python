from tesuto.models import BaseModel, types


class Script(BaseModel):
    name = types.StringType()
    framework_id = types.IntType()
    framework_name = types.StringType()
    module = types.StringType()
    parse_params = types.StringType()
    state_params = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'scripts'
        __mapper__ = 'data'


class ScriptFramework(BaseModel):
    name = types.StringType()
    module = types.StringType()

    class Options(BaseModel.Options):
        __resource__ = 'scripts/frameworks'
        __mapper__ = 'data'
